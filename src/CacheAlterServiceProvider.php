<?php

namespace Drupal\cache_alter;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the Page Cache service.
 */
class CacheAlterServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    try {
      $page_cache = $container->getDefinition('http_middleware.page_cache');
    }
    catch (\Exception $e) {
      // Do Nothing.
    }
    if (is_object($page_cache)) {
      $page_cache->setClass('Drupal\cache_alter\StackMiddleware\CacheAlter');
    }

  }

}
