<?php

namespace Drupal\cache_alter\StackMiddleware;

use Drupal\page_cache\StackMiddleware\PageCache;
use Symfony\Component\HttpFoundation\Request;

/**
 * Extending PageCache.
 */
class CacheAlter extends PageCache {

  /**
   * {@inheritdoc}
   */
  protected function getCacheId(Request $request) {
    if (!isset($this->cid)) {
      // Cache Keys Setup.
      $cid_parts = [
        $request->getSchemeAndHttpHost() . $request->server->get('REQUEST_URI'),
        $request->getRequestFormat(NULL),
        $request->cookies->get('cache_context'),
      ];
      $this->cid = implode(':', $cid_parts);
    }
    return $this->cid;
  }

}
