<?php

namespace Drupal\cache_alter\StackMiddleware;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Extending PageCache.
 */
class ClearRequest implements HttpKernelInterface {

  /**
   * The kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * Query MASK.
   *
   * @var array
   */
  protected $queryMask;

  /**
   * Constructs the UtmDummyMiddleware object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   */
  public function __construct(HttpKernelInterface $http_kernel) {
    $this->httpKernel = $http_kernel;
    $this->queryMask = [
      'utm_source',
      'utm_medium',
      'utm_campaign',
      'utm_term',
      'utm_content',
      'gclid',
      'yclid',
      'ysclid',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MAIN_REQUEST, bool $catch = TRUE) : Response {
    // Query clear.
    $query = $request->query;
    $this->fixServer($request);
    $this->fixQuery($request);
    return $this->httpKernel->handle($request, $type, $catch);
  }

  /**
   * Request Fix Query.
   */
  public function fixQuery(Request $request) {
    $query = $request->query;
    foreach ($query->keys() as $key) {
      $keylower = strtolower($key);
      if (in_array($keylower, $this->queryMask)) {
        $query->remove($key);
      }
    }
  }

  /**
   * Request Fix Server.
   */
  public function fixServer($request) {
    $server = $request->server;
    // Parce URI.
    $uri = $request->server->get('REQUEST_URI');
    $components = parse_url($uri);
    $request_path = $this->getRequestPath($components);
    $query_string = $this->getQueryString($components);
    // Server setup.
    $server = $request->server;
    if ($query_string) {
      $request_path = "$request_path?$query_string";
    }
    $server->set('REQUEST_URI', $request_path);
    $server->set('QUERY_STRING', $query_string);
  }

  /**
   * Parse PATH.
   */
  private function getRequestPath($components) {
    $request_path = "";
    if (isset($components['path'])) {
      $request_path = $components['path'];
    }
    return $request_path;
  }

  /**
   * Parse QUERY.
   */
  private function getQueryString($components) {
    $query_string = "";
    if (isset($components['query'])) {
      $query_data = [];
      parse_str($components['query'], $query_data);
      if (!empty($query_data)) {
        foreach ($query_data as $key => $value) {
          $keylower = strtolower($key);
          if (in_array($keylower, $this->queryMask)) {
            unset($query_data[$key]);
          }
        }
      }
      if (!empty($query_data)) {
        $query_string = http_build_query($query_data);
      }
    }
    return $query_string;
  }

}
