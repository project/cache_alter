# CACHE_ALTER MODULE FOR DRUPAL

## Чем занимается?

### ru

Всё ради того, чтобы на каждый запрос по переходу из рекламы не формировался свой отдельный page_cage а юзался существующий

**В самом начале загрузки друпала модуль cache_alter чистит из $request->query ключи**

- utm_source
- utm_medium
- utm_campaign
- utm_term
- utm_content
- gclid
- yclid
- ysclid

  **Везде далее, в том числе и в dynamic_page_cache, кэш формируется без этих значений**

- При формировании `page_cage` берётся очищенный `$request->query`
- При формировании `page_cage` к ключу добавляется значение куки `cache_context`, которая позволяет формировать разный статик кэш в зависимости от значения в этой куке, например положить туда выбранный город.

### en

At the very beginning of loading Drupal cleans keys from `$request->query`

- utm_source
- utm_medium
- utm_campaign
- utm_term
- utm_content
- gclid
- yclid
- ysclid

**Everywhere further, including in dynamic_page_cache - is formed without these values**
- On the forming `page_cage`, a cleared `$request->query` is taken
- On the forming `page_cage`, the cookie value `cache_context` is added to the key, which allows you to create different static cache depending on the value in this cookie, for example, put the selected city there.

## Расположение

- /modules/contrib

## Машинное имя модуля

- cache_alter

## Требования к платформе

- _Drupal 10-11_
- _PHP 7.4.0+_

## Нужны модули

## Версии

- [Drupal.org prod версия](https://www.drupal.org/project/cache_alter)

```sh
composer require 'drupal/cache_alter'
```

## Как использовать?

## Основные сервисы

## Доп сервисы
